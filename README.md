# pclone

`pclone` is a performant proxy-based object cloner for JavaScript. 
It follows a Copy-On-Write (CoW) model, allowing for efficient 
object modifications while preserving the original object's integrity. 
Unlike deep copying, `pclone` doesn't duplicate the original object so
it takes up significantly less RAM, making it a more performant 
alternative when working with large amounts of data.

## Installation

```bash
npm install pclone
```

## Usage

```javascript
const pclone = require('pclone');

let originalObject = {
  name: "John",
  age: 30,
  address: {
    city: "New York",
    country: "USA"
  }
};

let cloneObj = pclone(originalObject);

// Modify the cloned object
cloneObj.address.city = "San Francisco";

// originalObject remains unaffected
console.log(originalObject.address.city); // Output: New York

// Peek into the cloned object to see the changes
let peekAtClone = pclone.peek(cloneObj);
console.log(peekAtClone.get_clone_data()); // Output: { address: { city: 'San Francisco' } }
```

## Features

- **Performant**: `pclone` is designed for performance. It avoids the memory bloat associated with deep copying, making it a faster and more memory-efficient solution for managing object modifications.
  
- **Copy-On-Write (CoW)**: By adhering to the Copy-On-Write strategy, `pclone` only copies data when a write operation is performed, ensuring the original object remains untouched.

- **Peek into Changes**: With the `pclone.peek` function, you can easily track the modifications made to the cloned object without affecting the original object.

- **Dynamic Tracking of Original Object**: `pclone` maintains a live link between the clone and the original object. Any changes to the original object are automatically reflected in the clone, unless a particular property has already been modified in the clone. This dynamic tracking is invaluable in scenarios where the original data might be updated dynamically, ensuring that the cloned object stays synchronized with the latest state of the original object, while still preserving its own modifications. For instance, in a situation where a configuration object is updated dynamically, and multiple parts of your application work with a cloned version of this configuration, `pclone` ensures these parts always have access to the most current configuration data without losing their individual modifications.

## Examples

### Example 1: Preventing External Mutation

Imagine a scenario where you need to pass an object to an external library for processing, but you don't want the original object to be mutated.

```javascript
const externalLibraryProcess = require('external-library').process;

let processedObj = externalLibraryProcess(pclone(originalObject));

// originalObject remains unaffected, while processedObj contains the processed data
```

### Example 2: Efficient Data Modification

When working with large datasets, creating deep copies for every modification can be very inefficient. `pclone` provides a more efficient solution.

```javascript
let largeDataset = getLargeDataset(); // Assume this function returns a large dataset

let modifiedDataset = pclone(largeDataset);
modifiedDataset.newField = 'new data';

// largeDataset remains unaffected, while modifiedDataset contains the new data
```

### Example 3: Tracking Changes

If you need to track changes made to an object over time, `pclone` makes this easy.

```javascript
let trackedChanges = pclone(originalObject);
trackedChanges.name = "Doe";

let changes = pclone.peek(trackedChanges);
console.log(changes.get_clone_data()); // Output: { name: "Doe" }
```

### Example 4: Dynamic Tracking of Original Object

With `pclone`, the cloned object continues to track changes in the original object, unless the property has already been modified in the clone. This can be highly useful in scenarios where the original data might be updated dynamically.

```javascript
const pclone = require('pclone');

let originalConfig = {
  server: {
    host: 'localhost',
    port: 8080
  }
};

let clonedConfig = pclone(originalConfig);

// An external update modifies the original config
externalUpdate(originalConfig);

function externalUpdate(config) {
  config.server.port = 9090;
}

// The change in the original object is reflected in the cloned object
console.log(clonedConfig.server.port);  // Output: 9090

// However, if we had already modified the port in the cloned object, 
// the external update wouldn't affect the cloned object
clonedConfig.server.port = 3000;
externalUpdate(originalConfig);
console.log(clonedConfig.server.port);  // Output: 3000
```

This dynamic tracking functionality ensures that the cloned object stays updated with the latest data from the original object while preserving its own modifications, providing a balance between data synchronization and data integrity.

### Example 5: Debugging Unexpected Object Modifications

`pclone` is a powerful tool for debugging, especially when you are dealing with functions that should not modify your objects but do so unexpectedly. By creating a clone before passing the object to a function, and then examining any differences afterward, you can uncover unexpected modifications.

```javascript
const pclone = require('pclone');

let userData = {
  name: "John Doe",
  age: 30,
  preferences: {
    theme: "dark",
    language: "en-US"
  }
};

// Suppose we have a function viewPreferences that should only read userData
function viewPreferences(user) {
  // ... some operations
  user.preferences.theme = "light";  // unexpected modification
}

// Before calling the function, create a clone
let clonedData = pclone(userData);

// Call the function
viewPreferences(userData);

// Now use pclone.peek to see what was modified, if anything
let peekAtClone = pclone.peek(clonedData);
console.log(peekAtClone.get_clone_data());  // Output: { preferences: { theme: 'light' } }
```

In this example, the `viewPreferences` function is supposed to only read the `userData` object but it unexpectedly modifies it. `pclone` is used to detect this unexpected modification, showcasing how it can be employed to ensure that functions are behaving as expected with respect to data modification.

This way, `pclone` helps to expose unexpected modifications to your object, providing a clear insight into how your functions are interacting with your data, and aiding in debugging and ensuring data integrity.

```


## License

MIT

