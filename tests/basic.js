const assert = require('assert');
const pclone = require('../pclone.js');

describe('pclone', function() {

  it('should create a clone', function() {
    const original = { a: 1, b: { c: 2 } };
    const clone = pclone(original);
    
    // Change clone, should not affect original
    clone.b.c = 3;
    assert.strictEqual(original.b.c, 2);
  });

  it('should reflect changes in the original object unless overridden', function() {
    const original = { a: 1, b: { c: 2 } };
    const clone = pclone(original);
    
    // Change original, should reflect in clone
    original.b.c = 3;
    assert.strictEqual(clone.b.c, 3);
    
    // Override change in clone, should not reflect further changes from original
    clone.b.c = 4;
    original.b.c = 5;
    assert.strictEqual(clone.b.c, 4);
  });

  it('should provide peek functionality to view changes', function() {
    const original = { a: 1, b: { c: 2 } };
    const clone = pclone(original);
    
    // Change clone
    clone.b.c = 3;
    
    // Peek at changes
    const peek = pclone.peek(clone);
    assert.deepStrictEqual(peek.get_clone_data(), { b: { c: 3 } });
  });

});

