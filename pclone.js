const PCLONE_INFO = Symbol.for('___pclone_info');
function pclone(parent) {
    // prevent an attempt to pclone null or undefined;
    if (typeof parent !== 'object') {
        return parent;
    } else if (parent === null) {
        // if typeof IS object, but parent == undefined then we have null
        // so we return a new null
        return null;
    }

    // if the parent object has it's own create_pclone function, we use it.
    // First we check to see if we are an already pcloned object of the raw variety, if not, then we
    // check if we have create_pclone.  This saves us from a potentially costly deep-dive into the
    // localization tree.

    if (!Object.prototype.hasOwnProperty.call(parent, PCLONE_INFO) && typeof parent.create_pclone === 'function') {
        return parent.create_pclone(parent);
    } else {
        // We need to do a proxy create.
        let proxy_data = {};
        let white_out = {};
        let pclone_info = {
            real_item: parent
        };

        if (Array.isArray(parent)) {
            pclone_info.is_array = true;
            proxy_data.length = parent.length;
            Object.setPrototypeOf(proxy_data, Array.prototype);
        } else {
            pclone_info.is_array = false;
        }

        // Buffer gets confused with proxied buffers,
        // so if we are proxying a buffer, we have to copy it. :-(
        if (Buffer && Buffer.isBuffer(parent)) {
            let b = new Buffer(parent.length);
            parent.copy(b);
            return b;
        }

        let handlers, new_proxy, parent_proto;
        if (typeof Proxy == 'function') {
            //       8888b.  88 88""Yb 888888  dP""b8 888888
            //        8I  Yb 88 88__dP 88__   dP   `"   88
            //        8I  dY 88 88"Yb  88""   Yb        88
            //       8888Y"  88 88  Yb 888888  YboodP   88
            //
            //        88""Yb 88""Yb  dP"Yb  Yb  dP Yb  dP
            //        88__dP 88__dP dP   Yb  YbdP   YbdP
            //        88"""  88"Yb  Yb   dP  dPYb    8P
            //        88     88  Yb  YbodP  dP  Yb  dP
            //
            // .dP"Y8 88   88 88""Yb 88""Yb  dP"Yb  88""Yb 888888
            // `Ybo." 88   88 88__dP 88__dP dP   Yb 88__dP   88
            // o.`Y8b Y8   8P 88"""  88"""  Yb   dP 88"Yb    88
            // 8bodP' `YbodP' 88     88      YbodP  88  Yb   88
            //
            // This is proxy support for node 6.0.0+ using the Direct Proxy API.
            handlers = {
                getOwnPropertyDescriptor: function(target, name) {
                    let desc, new_desc;
                    if (proxy_data.hasOwnProperty(name) || white_out.hasOwnProperty(name) ) {
                        // console.log("returning proxy");
                        desc = Object.getOwnPropertyDescriptor(proxy_data, name);
                    } else {
                        // console.log("returning target");
                        desc = Object.getOwnPropertyDescriptor(target, name);
                    }

                    if (pclone_info.is_array && name == 'length') {
                        // console.log('length!!');
                        new_desc = {};
                        for (let key in desc) {
                            if (desc.hasOwnProperty(key)) {
                                new_desc[key] = desc[key];
                            }
                        }
                        new_desc.value = proxy_data['length']; // desc.value;
                        new_desc.writable = true;
                        new_desc.enumerable = false;
                        new_desc.configurable = false;
                        desc = new_desc;
                    }

                    return desc;
                },
                ownKeys: function(target) {
                    let keys ={};

                    let all_keys = Object.getOwnPropertyNames(target).concat(Object.getOwnPropertyNames(proxy_data));
                    let keys_to_return = [];
                    let total_keys = all_keys.length;

                    for (let i = 0; i < total_keys; i++) {
                        if (keys[all_keys[i]] === undefined && white_out.hasOwnProperty(all_keys[i]) !== true) {
                            keys_to_return.push(all_keys[i]);
                            keys[all_keys[i]] = 1;
                        }
                    }
                    return keys_to_return;
                },
                defineProperty: function(target, name, desc) {
                    Object.defineProperty(proxy_data, name, desc);
                },
                deleteProperty: function(target, name) {
                    // console.warn('deleting ', name);
                    // if we delete from the proxy, we need to hide the contents of the parent.
                    // so we need some white out.
                    white_out[name] = 1;
                    return delete proxy_data[name];
                },
                get: function(target, name, proxy) {

                    if (name == PCLONE_INFO) {
                        return pclone_info;
                    }

                    if (white_out.hasOwnProperty(name)) {
                        return undefined;
                    } else if (proxy_data.hasOwnProperty(name)) {
                        return proxy_data[name];
                    } else {

                        let val;

                        // It is VERY important that this happens exactly this way.
                        // if you try to test typeof parent[name] - you wind up with
                        // an exponential check on multi-level pclone.
                        // Get the value first, then decide if it needs to be pcloned.
                        val = parent[name];
                        if (typeof val === 'object') {
                            val = pclone(val);
                            proxy_data[name] = val;
                        } else if (typeof val == 'function') {
                            return val.bind(proxy_data);
                        }
                        return val;
                    }

                },
                set: function(target, name, val, proxy) {
                    if (pclone_info.is_array) {
                        // if we have an array, setting a numeric property
                        let index = parseInt(name, 10);
                        if (!isNaN(index) && proxy_data['length'] <= index) {
                            proxy_data['length'] = index+1;
                        }
                    }
                    if (name == PCLONE_INFO) {
                        pclone_info = val;
                    } else {
                        proxy_data[name] = val;
                        if (white_out[name]) {
                            delete white_out[name];
                        }
                    }
                    return true;
                }
            }
            new_proxy = new Proxy(parent, handlers);


        } else {
            // 88  88    db    88""Yb 8b    d8  dP"Yb  88b 88 Yb  dP
            // 88  88   dPYb   88__dP 88b  d88 dP   Yb 88Yb88  YbdP
            // 888888  dP__Yb  88"Yb  88YbdP88 Yb   dP 88 Y88   8P
            // 88  88 dP""""Yb 88  Yb 88 YY 88  YbodP  88  Y8  dP
            //
            //        88""Yb 88""Yb  dP"Yb  Yb  dP Yb  dP
            //        88__dP 88__dP dP   Yb  YbdP   YbdP
            //        88"""  88"Yb  Yb   dP  dPYb    8P
            //        88     88  Yb  YbodP  dP  Yb  dP
            //
            // .dP"Y8 88   88 88""Yb 88""Yb  dP"Yb  88""Yb 888888
            // `Ybo." 88   88 88__dP 88__dP dP   Yb 88__dP   88
            // o.`Y8b Y8   8P 88"""  88"""  Yb   dP 88"Yb    88
            // 8bodP' `YbodP' 88     88      YbodP  88  Yb   88
            //
            // Node 6 uses direct proxies. Previous versions used
            // harmony proxies.
            // This is the old harmony_proxies support.
            // This code supports node 0.8 through 4.x

            if (Object.prototype.hasOwnProperty.call(parent, PCLONE_INFO)) {
                parent_proto = pclone_info.parent_proto;
            } else {
                parent_proto = Object.getPrototypeOf(parent);
            }

            let harmony_handlers = {};

            // harmony_proxy getOwnPropertyDescriptor
            harmony_handlers.getOwnPropertyDescriptor = function(name) {
                let desc;
                if (proxy_data.hasOwnProperty(name) || white_out.hasOwnProperty(name)) {
                    desc = Object.getOwnPropertyDescriptor(proxy_data, name);
                } else {
                    desc = Object.getOwnPropertyDescriptor(parent, name);
                }
                if (desc !== undefined) {
                    desc.configurable = true;
                }
                let new_desc;
                if (pclone_info.is_array && name == 'length') {
                    new_desc = {};
                    for (let key in desc) {
                        if (desc.hasOwnProperty(key)) {
                            new_desc[key] = desc[key];
                        }
                    }
                    new_desc.value = proxy_data['length'];
                    new_desc.writable = true;
                    new_desc.enumerable = false;
                    new_desc.configurable = false;
                    desc = new_desc;
                }
                return desc;
            };

            harmony_handlers.getPropertyDescriptor = harmony_handlers.getOwnPropertyDescriptor;

            // harmony_proxy getOwnPropertyNames
            harmony_handlers.getOwnPropertyNames = function() {
                let keys = {};
                let all_keys = Object.getOwnPropertyNames(parent).concat(Object.getOwnPropertyNames(proxy_data));
                let keys_to_return = [];
                let total_keys = all_keys.length;

                for (let i = 0; i < total_keys; i++) {
                    if (keys[all_keys[i]] === undefined && white_out.hasOwnProperty(all_keys[i]) !== true) {
                        keys_to_return.push(all_keys[i]);
                        keys[all_keys[i]] = 1;
                    }
                }
                return keys_to_return;
            };
            harmony_handlers.getPropertyNames = harmony_handlers.getOwnPropertyNames;

            // harmony_proxy defineProperty
            harmony_handlers.defineProperty = function(name, desc) {
                Object.defineProperty(proxy_data, name, desc);
            };

            // harmony_proxy delete
            harmony_handlers.delete = function(name) {
                white_out[name] = 1;
                return delete proxy_data[name];
            };

            // harmony_proxy fix
            harmony_handlers.fix = function() {
                let result = {};
                if (Object.is_frozen(proxy_data)) {
                    let proxy_keys = Object.getOwnPropertyNames(proxy_data);
                    let parent_keys = Object.getOwnPropertyNames(parent);
                    for (let i = 0; i < proxy_keys.length; i++) {
                        if (result[proxy_keys[i]] === undefined && white_out[proxy_keys[i]] !== 1) {
                            result[proxy_keys[i]] = Object.getOwnPropertyDescriptor(proxy_data, proxy_keys[i]);
                        }
                    }
                    for (i = 0; i < parent_keys.length; i++) {
                        if (result[parent_keys[i]] === undefined && white_out[parent_keys[i]] !== 1) {
                            result[parent_keys[i]] = Object.getOwnPropertyDescriptor(parent, parent_keys[i]);
                        }
                    }
                    return result;
                } else {
                    return undefined;
                }
            };
            // harmony_proxy get
            harmony_handlers.get = function(proxy, name) {
                if (name == PCLONE_INFO) {
                    return pclone_info;
                }

                if (white_out.hasOwnProperty(name)) {
                    return undefined;
                } else if (proxy_data.hasOwnProperty(name)) {
                    return proxy_data[name];
                } else {
                    let val;

                    // It is VERY important that this happens exactly this way.
                    // if you try to test typeof parent[name] - you wind up with
                    // an exponential check on multi-level localization.
                    // Get the value first, then decide if it needs localization.
                    val = parent[name];
                    if (typeof val === 'object') {
                        val = pclone(val);
                        proxy_data[name] = val;
                    }
                    return val;
                }

            };
            // harmony_proxy set
            harmony_handlers.set = function(proxy, name, val) {
                if (pclone_info.is_array) {
                    let index = parseInt(name, 10);
                    if (!isNaN(index) && proxy_data['length'] <= index) {
                        proxy_data['length'] = index+1;
                    }
                }
                if (name == PCLONE_INFO) {
                    pclone_info = val;
                } else {
                    proxy_data[name] = val;
                    if (white_out[name]) {
                        delete white_out[name];
                    }
                }
                return true;
            };

            new_proxy = Proxy.create(harmony_handlers, parent_proto);
        }


        pclone_info.is_parent = function(obj) {
            return (obj === parent);
        }

        pclone_info.get_clone_data = function() {
            let out = {};
            let attr, i;

            let proxy_keys = Object.keys(proxy_data);
            let pclone_info;
            for (i = 0; i < proxy_keys.length; i++) {
                pclone_info = undefined;
                attr = proxy_keys[i];
                if (attr === 'get_clone_data' || attr === PCLONE_INFO) {
                    continue;
                }
                // null is a special case, we only return
                // that if the proxy value is null and original value was not null
                if (proxy_data[attr] === null) {
                   if (parent[attr] !== null) {
                       out[attr] = proxy_data[attr];
                   } 
                   continue;
                }
                
                pclone_info = proxy_data[attr][PCLONE_INFO];
                if (typeof proxy_data[attr] === 'object' && typeof pclone_info === 'object' &&
                    (typeof pclone_info['is_parent'] && pclone_info.is_parent(parent[attr])) &&
                    typeof pclone_info['get_clone_data'] === 'function') {
                    // Q: if we have an array, do we deal with it differently?
                    // the normal object way of handling it answers correctly, but it gives
                    // an object back. Should we give the entire modified array? --jayk
                    // A: The expectation is you get what is different. This does that. --jayk
                    if (Array.isArray(proxy_data[attr]) ) {
                        let diff = {};
                        let objectydiff = pclone_info.get_clone_data();
                        for (let ind in objectydiff ) {
                            if (ind != 'length' &&  !isNaN(ind)) {
                                diff[ind] = objectydiff[ind];
                            }
                        }
                        out[attr] = diff;
                    } else {
                        out[attr] = pclone_info.get_clone_data();
                    }
                } else {
                    out[attr] = proxy_data[attr];
                }

            }
            let whiteout_keys = Object.keys(white_out);
            for (i = 0; i < whiteout_keys.length; i++) {
                attr = whiteout_keys[i];
                out[attr] = undefined;
            }
            return out;
        };

        // Set the parent prototype so we can get it later without walking the
        // entire hierarchy. (prevents nasty CPU eating on deep localization)

        pclone_info.get_parent = function() { return pclone_info.real_item };

        return new_proxy;
    }
}

pclone.peek = function(obj) {
    if (!Object.prototype.hasOwnProperty.call(obj, PCLONE_INFO)) {
        return obj[PCLONE_INFO];
    } else {
        return undefined;
    }
}

module.exports = pclone;
